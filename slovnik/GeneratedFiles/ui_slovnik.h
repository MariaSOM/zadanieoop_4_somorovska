/********************************************************************************
** Form generated from reading UI file 'slovnik.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SLOVNIK_H
#define UI_SLOVNIK_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_slovnikClass
{
public:
    QWidget *centralWidget;
    QLineEdit *lineEdit;
    QLabel *label;
    QPushButton *pushButton;
    QListWidget *listWidget;
    QPushButton *pushButton_2;
    QLabel *label_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *slovnikClass)
    {
        if (slovnikClass->objectName().isEmpty())
            slovnikClass->setObjectName(QStringLiteral("slovnikClass"));
        slovnikClass->resize(504, 398);
        centralWidget = new QWidget(slovnikClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(150, 10, 241, 31));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 131, 31));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(400, 10, 93, 28));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 50, 481, 241));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(40, 300, 411, 28));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(40, 350, 431, 31));
        slovnikClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(slovnikClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 504, 26));
        slovnikClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(slovnikClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        slovnikClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(slovnikClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        slovnikClass->setStatusBar(statusBar);

        retranslateUi(slovnikClass);
        QObject::connect(pushButton, SIGNAL(clicked()), slovnikClass, SLOT(hladaj()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), slovnikClass, SLOT(zobraz_vyznam()));

        QMetaObject::connectSlotsByName(slovnikClass);
    } // setupUi

    void retranslateUi(QMainWindow *slovnikClass)
    {
        slovnikClass->setWindowTitle(QApplication::translate("slovnikClass", "slovnik", 0));
        label->setText(QApplication::translate("slovnikClass", "Zadajte h\304\276adan\303\275 v\303\275raz:", 0));
        pushButton->setText(QApplication::translate("slovnikClass", "h\304\276ada\305\245", 0));
        pushButton_2->setText(QApplication::translate("slovnikClass", "Zobraz v\303\275znam", 0));
        label_2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class slovnikClass: public Ui_slovnikClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SLOVNIK_H
