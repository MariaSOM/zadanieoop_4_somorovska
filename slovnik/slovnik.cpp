#include "slovnik.h"

slovnik::slovnik(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

slovnik::~slovnik()
{

}

void slovnik::hladaj()
{
	QMessageBox mbox;
	ui.listWidget->clear();
	vyznam.clear();
	vyraz = ui.lineEdit->text();
	QString vyrazupraveny = vyraz.replace(" ", "+");
	QString pomoc = "http://www.urbandictionary.com/define.php?term=" + vyrazupraveny;
	
	url.setUrl(QString(pomoc));
	ui.statusBar->showMessage(pomoc);
	
	reply = qnam.get(QNetworkRequest(url));
	connect(reply, &QNetworkReply::finished, this, &slovnik::citaj_stranku);
}

void slovnik::citaj_stranku()
{
	vyraz = ui.lineEdit->text();
	QString vysledok = reply->readAll();
	QMessageBox mbox;
	QStringList help = vysledok.split("div>");
	//bool stav = false;
	
	for (int i = 0; i < help.length(); i++)
	{
		if (help[i].contains("class='meaning'>"))
		{
			if (help[i].contains("There aren't any definitions"))
				{
					mbox.setText("There aren't any definitions for " + vyraz + " yet.");
					mbox.exec();
				}
			else
				{
					stav = true;
					vyznam.push_back(help[i]);
				}
		}
		if (help[i].contains("class='example'>"))
		{
			expl.push_back(help[i]);
		}
	}
	
	if (stav == true)
	{
		for (int i = 0;i < vyznam.count() && i < 5; i++)
			{
				QString nazov = vyraz + " " + QString::number(i + 1);
				ui.listWidget->addItem(nazov);
			//	mbox.setText(vyznam[i]);
			//	mbox.exec();
			}	 
	}	
} 

void slovnik::zobraz_vyznam()
{
	int i = ui.listWidget->currentRow();
	QMessageBox mbox;

	if (/*ui.lineEdit->text() != "" && */i >= 0 && i < 5 && stav == true)
	{
		mbox.setWindowTitle("Meaning: " + vyraz + " " + QString::number(i+1));
		mbox.setText(vyznam[i]);
		mbox.exec();
	}
}
