#ifndef SLOVNIK_H
#define SLOVNIK_H

#include <QtWidgets/QMainWindow>
#include "ui_slovnik.h"
#include <QNetworkAccessManager>
#include <QUrl>
#include <QtNetwork>
#include <qmessagebox.h>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QList>
#include <QStringList>
#include <QModelIndex>


class slovnik : public QMainWindow
{
	Q_OBJECT

public:
	slovnik(QWidget *parent = 0);
	~slovnik();
	
	public slots:	
		void hladaj();
		void citaj_stranku();
		void zobraz_vyznam();

private:
	Ui::slovnikClass ui;
	QUrl url;
	QNetworkAccessManager qnam;
	QNetworkReply *reply;
	QString vyraz;
	QList<QString> vyznam;
	QList<QString> expl;
	bool stav = false;

};


#endif // SLOVNIK_H
